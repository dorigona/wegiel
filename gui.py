from tkinter import *
from tkinter import filedialog
class root:
    """
    TODO
    """
    def __init__(self):
        self.create_window()

    def create_window(self):
        self.window = Tk()
        self.window.title="Wegiel"
        # Creating a button to search the file
        b1 = Button(
            self.window,
            text = "Open File",
            command = self.get_file_path
            )
        b1.pack()
        exit_btn= Button(
            self.window,
            text = "Create plot",
            command = self.window.destroy
            )
        exit_btn.pack()
        self.window.mainloop()
    def get_file_path(self):
        # Open and return file path

        self.file_path= filedialog.askopenfilename(
            title = "Select A File",
            filetypes = (("CSV Files","*.csv"),)
            )
        l1 = Label(
            self.window,
            text = "File path: " + self.file_path
            )
        l1.pack()


if __name__=="__main__":
    import pandas as pd
    from wegiel import cycles

    root=root()
    df =pd.read_csv(root.file_path, decimal=',')

    cyc=cycles(
    df,
    annealing_points=6,
    correction_minima=2,
    )

    cyc.plot("time", "temperature")





