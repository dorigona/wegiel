#import pandas to read data files
import pandas as pd

#import cycle class from wegiel lib
from wegiel import cycle

#put your given path, be aware of
#stupid windows paths!
# in windows use r"" strings like
# r"data\GC2_RT_350_RT_250_RT.csv"
# or just os.path.join("data", "ur_file")

path="data/GC2_RT_350_RT_250_RT.csv"

path=os.path.join(
    "data"
    "GC2_RT_350_RT_250_RT.csv"
    )

#or use glob to receive file list
# from glob import glob
paths=glob("data/*.csv")
dfs=[pd.read_csv(df_path, decimal=",") for df_path in paths]
# or in case of excel files uncomment these lines:
#paths=glob("data/*.xlsx")
# dfs=[pd.read_excel(df_path, decimal=",") for df_path in paths]
#read more about this method in pandas docs:
#https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_excel.
# html

# define your cycle class instances
cycles=[cycle(df, annealing_points=6, correction_minima=2) for df in dfs]

#plot time and temperature in first file
cycles[0].plot("time", "temp", save_path="img")
