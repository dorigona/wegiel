#import pandas to read data files
import pandas as pd

#import cycle class from wegiel lib
from wegiel import cycle

#put your given path, be aware of
#stupid windows paths!
# in windows use r"" strings like
# r"data\GC2_RT_350_RT_250_RT.csv"
# or just os.path.join("data", "ur_file")

path="data/GC2_RT_350_RT_250_RT.csv"

path=os.path.join(
    "data"
    "GC2_RT_350_RT_250_RT.csv"
    )


#read csv file from given path
df =pd.read_csv(path, decimal=',')

"""
you can use xlsx file from excel using:
df.read_xlsx(path, sheet_name=your_sheet_name (default 0),
decimal=your_decimal point read more from pandas docs)
LINK:
https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_excel.
html
"""


# define your cycle class instance
cyc=cycle(
    df,
    annealing_points=6,
    correction_minima=2,
    )


# plot two desired columns time and temperature by:
cyc.plot("time", "temp")

# save it by putting save_path argument
cyc.plot("time", "temp", save_path="img")

# plot desired columns by temperature by:
# if u want to save plots, just put
# save_path argument in plot method
# the case is not important here
# so TeMp==temp
desired_cols = [
    "time",
    "Sheet resistance",
    "restivity"
    ]

for column in desired_cols:
    cyc.plot(column, "temp")
    #or if u want to save use
    #method below instead:
    #cyc.plot(column, "temp", save_path="img")
