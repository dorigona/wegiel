import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.interpolate import interp1d
import matplotlib.patches as mpatches
from scipy.signal import argrelmax,argrelmin, argrelextrema, find_peaks
from tkinter import *
from tkinter import filedialog

class cycle:
    def __init__(self,
                 data_frame: pd.DataFrame,
                 annealing_points:int,
                 correction_minima:int):
        self.df=data_frame
        time_name=[x for x in self.df.columns if "time" in x.lower()][0]
        temperature_name=[x for x in self.df.columns if "temp" in x.lower()][0]
        self.temperature=self.df[temperature_name].round()
        self.time=self.df[time_name]


        self.annealing_points=annealing_points
        self.correction_minima=correction_minima
        self.find_cycles()
        #self.calculate_hall_resistance()

    def calculate_hall_resistance(self):
        """
        TODO
        """
        xf=[x for x in self.df.columns if "UACp".lower() in x.lower()][0]
        xd=[x for x in self.df.columns if "UCDn".lower() in x.lower()][0]

        self.hall_resistance=(self.df[xf]+self.df[xd])/2
        self.hall_resistance.name="Hall resistance"
        self.df = pd.concat([self.df, self.hall_resistance], axis=1)



    def find_cycles(self):
        """
        Function Written below
        """
        peaks,_ = find_peaks(self.temperature)
        self.local_maxima = list(peaks)
        throughs,_ = find_peaks(np.negative(self.temperature))
        local_minima = list(throughs)
        local_minima.insert(0,0)
        local_minima.append(self.time.index[len(self.time.index)-1])
        self.local_minima = local_minima

        self.count=len(self.local_maxima)
        self.find_heatings()
        self.find_coolings()
        self.find_annealings()

    def find_annealings(self):
        self.annealings=[]
        for ind_1st in self.local_maxima:
            start=ind_1st-2
            stop=ind_1st-2+self.annealing_points
            self.annealings.append((start,stop))

    def find_coolings(self):


        self.coolings=[]
        for ind_1st, ind_last in zip(self.local_maxima, self.local_minima[1:]):
            start=ind_1st-3+self.annealing_points
            stop=ind_last+1
            self.coolings.append((start,stop))

    def find_heatings(self):
        self.heatings=[]
        for ind_1st, ind_last in zip(self.local_minima, self.local_maxima):
            start,stop=ind_1st, ind_last-1
            self.heatings.append((start,stop))

    def plot_cycle(self, x,
                   y,
                   index:int,
                   type_:str,
                  color: str,
        ):
        df=self.df
        points=getattr(self,type_)
        point=points[index]
        #for point in points:
        #if points.index(point)==0:
        plt.scatter(x[point[0]:point[1]],
        y[point[0]:point[1]],
        color=color,
        label=type_[:-1])

        self.title=f"{index+1}: "
        self.title+=f"{self.temperature[point[0]]:.0f}"
        self.title+=f"\N{DEGREE SIGN}C"



        plt.title(self.title)


                #else:
            #plt.plot(x[point[0]:point[1]],
            #y[point[0]:point[1]],
            #color=color)
            #plt.show()

    def plot(self, desired_x, desired_y, save_path=None, extension="png"):
        import os,re
        df=self.df
        col_x=[x for x in df.columns if desired_x.lower() in x.lower()]
        if len(col_x)>1:
            err_msg=f"Found {len(col_x)} columns searching by "
            err_msg+=f"argument: {desired_x}! "
            err_msg+="Make request more precise"
            raise AttributeError(err_msg)
        col_y=[x for x in df.columns if desired_y.lower() in x.lower()]
        if len(col_y)>1:
            err_msg=f"Found {len(col_y)} columns searching by "
            err_msg+=f"argument: {desired_y}! "
            err_msg+="Make request more precise"
            raise AttributeError(err_msg)
        x=df[col_x[0]]
        y=df[col_y[0]]
        #x=getattr(self, x)
        #y=getattr(self, y)
        #plt.plot(x, y)

        plt.ylabel(y.name)
        plt.xlabel(x.name)


        for i in range(len(self.heatings)):
            self.plot_cycle(x,y,i, "heatings", "red")
            self.plot_cycle(x,y,i, "coolings", "blue")
            self.plot_cycle(
                x,y,
                i,
                "annealings", "green"
            )

            title=self.title
            title=title.replace(":","")
            title=title.replace(f"\N{DEGREE SIGN}C","")
            title=title.replace(" ","_")



            xd=re.sub("\(.*?\)|\[.*?\]","",col_x[0])
            cxd=re.sub("\(.*?\)|\[.*?\]","",col_y[0])
            filename=f"{xd}_{cxd}_{title}"

            plt.legend(fontsize=20)
            plt.ylabel(y.name)
            plt.xlabel(x.name)
            if save_path is not None:
                s=os.path.join(
                    save_path,
                    f"{filename}.{extension}"
                    )
                try:
                    plt.savefig(s)
                except FileNotFoundError:
                    os.makedirs(save_path, exist_ok=True)
                    plt.savefig(s)
                plt.close()
            else:
                plt.show()

