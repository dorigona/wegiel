# Why use wegiel?

wegiel is a library to easier manage readouts from Hall effect tests. Just
import your data into pandas dataframe using `pandas.read_*` methods from
pandas lib.

The docs are in preparation, as well as PyPI page.


```python
from wegiel import cycle
df=pd.DataFrame()

cyc=cycle(
    df,
    annealing_points=6,
    correction_minima=2,
    )
```

# How to use

to use it just copy-paste `wegiel.py` file into your folder and next just put.

before you need to install `scipy`, `numpy`, `matplotlib` and `pandas` by:

```bash
pip install scipy
pip install numpy
pip install pandas
pip install matplotlib
```

```python
from wegiel import cycle
```

`cycle` is the main library class

Find more in the `examples` folder
